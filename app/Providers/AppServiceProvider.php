<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\File;
use App\Models\Product;
use App\Models\Prop;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
     Relation::morphMap([
      'category' => Category::class,
      'file' => File::class,
      'product' => Product::class,
      'tag' => Tag::class,
      'user' => User::class,
      'prop' => Prop::class,
     ]);
    }
}
