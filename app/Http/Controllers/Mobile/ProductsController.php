<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Http\Resources\Mobile\Product\ProductCollection;
use App\Http\Resources\Mobile\Product\SingleProductResource;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;

class ProductsController extends Controller
{
 public function index()
 {

//return Category::with('products')->find(1);
//  return
  $products = Product::
  with([
   'categories',
   'files' => function ($q0) {
    return $q0->special();
   },
  ])
   ->when(request('q'), function ($query0, $q1) {
    return $query0
     ->where('name','like','%'.$q1.'%')
     ->orWhere('code','like','%'.$q1.'%');
   })
   ->when(request('category'), function (Builder $query1, $category) {
    return $query1
     ->whereHas('categories', function ($q) use($category){
      $q->where('categories.id', $category);
     });
   })
   ->get();
  return new ProductCollection($products);
 }

 public function show($id)
 {
  $product = Product::with(['files', 'categories', 'tags'])->findOrFail($id);
  return new SingleProductResource($product);
 }


}