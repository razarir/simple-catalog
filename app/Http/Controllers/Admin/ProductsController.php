<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Product\StoreProductRequest;
use App\Http\Requests\Admin\Product\UpdateProductRequest;
use App\Http\Resources\Admin\Product\ProductCollection;
use App\Http\Resources\ProductResourceAdmin\Product\EditProductResource;
use App\Models\File;
use App\Models\Product;
use Illuminate\Http\Request;
use Validator;

class ProductsController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  $products = Product:: with([
   'categories',
   'files' => function ($q) {
    return $q->special();
   },
  ])
//   ->first();
   ->get();
//   ->paginate(20);
//  dump($products);

//  return $products;
  return new ProductCollection($products);
//  return new ProductResource::collection($products);
//  return new ProductResource($products);
//  return view('admin.master',compact('products'));
//  return view('admin.pages.product.index',compact('products'));
//  return view('admin.pages.product.index',compact('products'));
//  return Product::with('props', 'tags', 'categories', 'files')->get();
 }

 /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function create()
 {
  return view('admin.pages.product.index');

 }

 /**
  * Store a newly created resource in storage.
  *
  * @param Request $request
  * @return \Illuminate\Http\Response
  */
 public function store(StoreProductRequest $request)
 {

  $product = Product::create(request()->only([
   'name',
   'price',
   'description',
   'code',
  ]));

  if ($selectedTags = request('selected_tags')) {
   foreach ($selectedTags as $selectedTag) {
    if (isset($selectedTag->tag)) {
     if (isset($selectedTag->val)) {
      $readySelectedTags[$selectedTag->tag] = ['value' => $selectedTag->val];
     }
    }
   }
   $product->tags()->attach($readySelectedTags);
  }
  if ($categories = request('categories')) {
   $product->categories()->attach($categories);
  }
  if (request()->file('files') && $infoFiles = request('files_info')) {
   foreach (request()->file('files') as $key => $file) {
    /**
     * @var $file Illuminate\Http\UploadedFile
     */
    $address = $file->store('/public/products/' . $product->id);
    $attributes = collect($infoFiles[$key])
     ->prepend($infoFiles[$key]->type, 'mime_type')
     ->prepend($infoFiles[$key]->special, 'special')
     ->prepend($address, 'address')
     ->toArray();
    $product
     ->files()
     ->create($attributes);
   }
  }
  return $product;
 }

 /**
  * Display the specified resource.
  *
  * @param Product $product
  * @return \Illuminate\Http\Response
  */
 public function show(Product $product)
 {
  return $product;
 }

 /**
  * Show the form for editing the specified resource.
  *
  * @param Product $product
  * @return \Illuminate\Http\Response
  */
 public function edit(Product $product)
 {
//  return $product->load(['files', 'categories', 'tags']);
  return new EditProductResource($product->load(['files', 'categories', 'tags']));
 }

 /**
  * Update the specified resource in storage.
  *
  * @param Request $request
  * @param Product $product
  * @return \Illuminate\Http\Response
  */
 public function update(UpdateProductRequest $request, Product $product)
 {
  $product->update(request()->only([
   'name',
   'price',
   'description',
   'code',
  ]));
  if ($selectedTags = request('selected_tags')) {
   $readySelectedTags = [];
   foreach ($selectedTags as $selectedTag) {
    if (isset($selectedTag->tag)) {
     if (isset($selectedTag->val)) {
      $readySelectedTags[$selectedTag->tag] = ['value' => $selectedTag->val];
     }
    }
   }
   $product->tags()->sync($readySelectedTags);
  }
  if ($categories = request('categories')) {
   $product->categories()->sync($categories);
  }
  $list = [];
  if ($infoFiles = request('files_info')) {
   if (request()->file('files')) {
    foreach ($infoFiles as $info_file) {
     foreach (request()->file('files') as $key => $file) {
      if ($this->this_info_file_is_of_the_this_file($file, $info_file)) {
       $list[] = [
        'file' => $file,
        'info' => $info_file
       ];
      }
     }
    }
//بقیه مواردی که اضافه نشدن
    foreach ($infoFiles as $info_file) {
     $isAdded = false;
     foreach ($list as $item) {
      if ($info_file === $item['info']) {
       $isAdded = true;
      }
     }

     if ($isAdded) {
      break;
     }
     else {
      $list[] = [
       'info' => $info_file
      ];
     }
    }
   }
   else {
    foreach ($infoFiles as $info_file) {
     $list[] = [
      'info' => $info_file
     ];
    }
   }
//ذخیره سازی
   foreach ($list as $value) {
    $info_file = $value['info'];
    if (isset($value['file'])) {
     $file = $value['file'];
     /**
      * @var $file Illuminate\Http\UploadedFile
      */
     $address = $file->store('/public/products/' . $product->id);
     $attributes = collect($info_file)
      ->prepend($address, 'address')
      ->prepend($info_file->type, 'mime_type')
      ->prepend($info_file->special, 'special')
      ->toArray();
     $product
      ->files()
      ->create($attributes);
    }
    else {
     if ($info_file->deleted) {
      $file1 = File::findOrFail($info_file->id);
      \Storage::delete('public' . \Str::after($file1->address, '/storage'));
      $file1->delete();
     }
     else {
      File::whereId($info_file->id)->update([
       'special' => $info_file->special
      ]);
     }
    }
   }
  }

 }

 /**
  * Remove the specified resource from storage.
  *
  * @param Product $product
  * @return \Illuminate\Http\Response
  */
 public function destroy(Product $product)
 {
  \Storage::deleteDirectory('public/products/' . $product->id);
  $product->files()->delete();
  return $product->delete();
 }

 public function this_info_file_is_of_the_this_file(\Illuminate\Http\UploadedFile $file, $info_file): bool
 {
  return $file->getSize() === $info_file->size && $file->getMimeType() === $info_file->type && $file->getClientOriginalName() === $info_file->name;
 }

 /**
  * @param $info_file
  * @throws \Exception
  */
 private function removeFile($info_file): void
 {
  $file1 = File::findOrFail($info_file->id);
  \Storage::delete('public' . \Str::after($file1->address, '/storage'));
  $file1->delete();
 }

 /**
  * @param $info_file
  */
 private function updateFile($info_file): void
 {
  File::whereId($info_file->id)->update([
   'special' => $info_file->special
  ]);
 }

 /**
  * @param \Illuminate\Http\UploadedFile $file
  * @param Product $product
  * @param $info_file
  * @param \Illuminate\Contracts\Foundation\Application|null $infoFiles
  * @param int $key
  */
 private function createFile(\Illuminate\Http\UploadedFile $file, Product $product, $info_file, $infoFiles, int $key): void
 {
//  dump($info_file,'1111111111',$infoFiles);
  $address = $file->store('/public/products/' . $product->id);
  $attributes = collect($info_file)
   ->prepend($address, 'address')
   ->prepend($info_file->type, 'mime_type')
   ->prepend($info_file->special, 'special')
   ->toArray();
  $product
   ->files()
   ->create($attributes);
 }


}