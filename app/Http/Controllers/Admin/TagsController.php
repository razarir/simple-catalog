<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Tag\StoreTagRequest;
use App\Http\Requests\Admin\Tag\UpdateTagRequest;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class TagsController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  return Tag::when(request('with_img'), function (Builder $q, $value) {
   return $q->with('image');
  })
   ->get();
 }

 /**
  * Store a newly created resource in storage.
  *
  * @param Request $request
  * @return \Illuminate\Http\Response
  */
 public function store(StoreTagRequest $request)
 {
  $tag = Tag::create($request->only([
   'code',
   'description',
   'name',
  ]));
  $file = \request()->file('files')[0];
  $address = $file->store('/public/tag/' . $tag->id);
  $tag
   ->image()
   ->create([
    'address' => $address,
    'name' => $file->getClientOriginalName(),
    'mime_type' => $file->getMimeType(),
    'extension' => $file->extension(),
    'size' => $file->getSize(),
    'special' => true
   ]);

  return $tag->load('image');
 }

 /**
  * Display the specified resource.
  *
  * @param Tag $tag
  * @return \Illuminate\Http\Response
  */
 public function show(Tag $tag)
 {
  //
 }

 /**
  * Update the specified resource in storage.
  *
  * @param Request $request
  * @param Tag $tag
  * @return \Illuminate\Http\Response
  */
 public function update(UpdateTagRequest $request, Tag $tag)
 {

  if (\request('files')) {
   \Storage::delete('public' . \Str::after($tag->image->address, '/storage'));
   $file = \request()->file('files')[0];
   $address = $file->store('/public/tag/' . $tag->id);
   $tag
    ->image()
    ->updateOrCreate([
     'fileable_id' => $tag->id,
     'fileable_type' => 'tag'
    ], [
     'address' => $address,
     'name' => $file->getClientOriginalName(),
     'mime_type' => $file->getMimeType(),
     'extension' => $file->extension(),
     'size' => $file->getSize(),
     'special' => true
    ]);
  }
  $tag->update([
   'code' => \request('code'),
   'description' => \request('description'),
   'name' => \request('name'),
  ]);
  return $tag->load('image');
 }

 /**
  * Remove the specified resource from storage.
  *
  * @param Tag $tag
  * @return \Illuminate\Http\Response
  */
 public function destroy(Tag $tag)
 {
  \Storage::deleteDirectory('public/tag/' . $tag->id);
  $tag->image()->delete();
  return $tag->delete();
 }
}