<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\StoreCategoryRequest;
use App\Http\Requests\Admin\Category\UpdateCategoryRequest;
use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  return Category::when(request('with_img'), function (Builder $q, $value) {
   return $q->with('image');
  })
   ->get();

 }

 /**
  * Store a newly created resource in storage.
  *
  * @param Request $request
  * @return \Illuminate\Http\Response
  */
 public function store(StoreCategoryRequest $request)
 {
  $category = Category::create($request->only([
   'code',
   'description',
   'name',
  ]));
  $file = \request()->file('files')[0];
  $address = $file->store('/public/category/' . $category->id);
  $category
   ->image()
   ->create([
    'address' => $address,
    'name' => $file->getClientOriginalName(),
    'mime_type' => $file->getMimeType(),
    'extension' => $file->extension(),
    'size' => $file->getSize(),
    'special' => true
   ]);

  return $category->load('image');

 }

 /**
  * Display the specified resource.
  *
  * @param Category $category
  * @return \Illuminate\Http\Response
  */
 public function show(Category $category)
 {
  //
 }

 /**
  * Update the specified resource in storage.
  *
  * @param Request $request
  * @param Category $category
  * @return \Illuminate\Http\Response
  */
 public function update(UpdateCategoryRequest $request, Category $category)
 {
//  return $request;

  if (\request('files')) {
   \Storage::delete('public' . \Str::after($category->image->address, '/storage'));
   $file = \request()->file('files')[0];
   $address = $file->store('/public/category/' . $category->id);
   $category
    ->image()
    ->updateOrCreate([
     'fileable_id' => $category->id,
     'fileable_type' => 'category'
    ], [
     'address' => $address,
     'name' => $file->getClientOriginalName(),
     'mime_type' => $file->getMimeType(),
     'extension' => $file->extension(),
     'size' => $file->getSize(),
     'special' => true
    ]);
  }
  $category->update([
   'code' => \request('code'),
   'description' => \request('description'),
   'name' => \request('name'),
  ]);
  return $category->load('image');

 }

 /**
  * Remove the specified resource from storage.
  *
  * @param Category $category
  * @return \Illuminate\Http\Response
  */
 public function destroy(Category $category)
 {
  \Storage::deleteDirectory('public/category/' . $category->id);
  $category->image()->delete();
  return $category->delete();
 }
}