<?php

namespace App\Http\Resources\Mobile\Category;

use App\Http\Resources\Mobile\Product\ProductCollection;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Category */
class SingleCategoryResource extends JsonResource
{
 /**
  * @param \Illuminate\Http\Request $request
  * @return array
  */
 public function toArray($request)
 {
  return [
   'id' => $this->id,
   'name' => $this->name,
   'description' => $this->description,
   'code' => $this->code,
   'special' => $this->special_with_default,
   'tags' => $this->tags->map(function ($tag) {
    return $tag->name.' : '.$tag->pivot->value;
   })
  ];
 }
}
