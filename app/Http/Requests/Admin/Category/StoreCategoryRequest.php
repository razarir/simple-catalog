<?php

namespace App\Http\Requests\Admin\Category;

use App\Models\File;
use Illuminate\Foundation\Http\FormRequest;

class StoreCategoryRequest extends FormRequest
{
 public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
 {
  parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
  (new \App\Lib\Request())->ready_request();

 }

 public function rules()
 {
  return [
   'files' => 'required|array',
   'code' => 'required|string|unique:categories',
   'description' => 'required|string',
   'name' => 'required|string|unique:categories',
  ];
 }

 public function authorize()
 {
  return true;
 }
}