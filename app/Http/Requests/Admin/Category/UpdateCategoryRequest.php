<?php

namespace App\Http\Requests\Admin\Category;

use App\Models\File;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateCategoryRequest extends FormRequest
{
 public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
 {
  parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
  (new \App\Lib\Request())->ready_request();

 }

 public function rules()
 {
  return [
   'code' => [
    'required', 'string', Rule::unique('categories', 'code')->ignore((integer)(request('id'))),
   ],
   'description' => 'required|string',
   'name' => [
    'required', 'string', Rule::unique('categories', 'name')->ignore((integer)(request('id'))),
   ],
  ];
 }

 public function authorize()
 {
  return true;
 }
}