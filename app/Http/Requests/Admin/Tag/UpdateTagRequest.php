<?php

namespace App\Http\Requests\Admin\Tag;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateTagRequest extends FormRequest
{
 public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
 {
  parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
  (new \App\Lib\Request())->ready_request();

 }

 public function rules()
 {
  return [
   'name' => [
    'required',
    'string',
    Rule::unique('tags', 'name')->ignore((integer)(request('id'))),
   ],
  ];
 }

 public function authorize()
 {
  return true;
 }
}