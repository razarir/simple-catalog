<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
 use HasFactory;

 protected $table = 'tags';
 public $timestamps = true;
 protected $fillable = array('name');

 public function products()
 {
  return $this->morphedByMany(Product::class, 'tagable')->withPivot('value');
 }

 public function image()
 {
  return $this->morphOne(File::class, 'fileable');
 }
}