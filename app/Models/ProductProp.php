<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductProp extends Model 
{

    protected $table = 'product_prop';
    public $timestamps = true;
    protected $fillable = array('product_id', 'prop_id', 'value');

}