<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Storage;

class File extends Model
{
 use HasFactory;

 protected $table = 'files';
 public $timestamps = true;
 protected $fillable = array('name', 'address', 'mime_type', 'size', 'thumbnail', 'extension', 'special', 'fileable_id', 'fileable_type', 'alt');

 protected $casts = [
  'Special' => 'boolean'
 ];

 public function fileable()
 {
  return $this->morphTo();
 }

 public function scopeSpecial($query)
 {
  return $query->where('Special', True);
 }

 public function defaultSpecial($type)
 {
  $list = [
   Product::class => 'default/product/placeholderfurniture.png',
   Category::class => 'default/category/placeholderfurniture.png',
  ];
  return Storage::url($list[$type]);
 }

 public function getAddressAttribute($value)
 {
  return Storage::url($value);
 }
}