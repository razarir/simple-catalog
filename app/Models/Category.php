<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
 use HasFactory;

 protected $table = 'categories';
 public $timestamps = true;
 protected $fillable = array('name', 'description', 'code', 'parent');

 public function products()
 {
  return $this->belongsToMany(Product::class);
 }

 public function tags()
 {
  return $this->morphToMany(Tag::class, 'tagable')->withPivot('value');
 }

 public function image()
 {
  return $this->morphOne(File::class, 'fileable');
 }

 public function getSpecialWithDefaultAttribute()
 {
  $file = $this->files->first();
  if ( $file=== null) {
   return (new File)->defaultSpecial(Category::class);
  }else{
   return $file->address;
  }
 }
}