<?php


namespace App\Lib;


class Request
{
 public static $request_data;
 public static $statusCode;

// const request_data =null;
 public function __construct()
 {
  $allData = collect(json_decode(request()->data));
  $this->assinge_file_to_info_file($allData);
  self::$request_data = $allData;
  self::$statusCode = 200;

 }

 /**
  * @param \Illuminate\Http\Request $request
  */
 public function ready_request()
 {
  foreach (json_decode(request('data')) as $key => $value) {
   request()->request->add([$key => $value]);
  }
  request()->request->remove('data');
 }


 public static function request_data()
 {
  return self::$request_data;
 }

 public static function update($update)
 {
  self::$request_data = $update;
 }

 /**
  * @param \Illuminate\Http\UploadedFile $file
  * @param $info_file
  * @return bool
  */
 public function this_info_file_is_of_the_this_file(\Illuminate\Http\UploadedFile $file, $info_file): bool
 {
  return $file->getSize() === $info_file->size && $file->getMimeType() === $info_file->type && $file->getClientOriginalName() === $info_file->name;
 }

 /**
  * @param \Illuminate\Support\Collection $data
  */
 public function assinge_file_to_info_file(\Illuminate\Support\Collection $data): void
 {

  if (isset($data['all_info_files'])) {
   foreach ($data['all_info_files'] as $info_file) {
    if (isset(request()->allFiles()[$info_file->belong_to])) {
     foreach (request()->allFiles()[$info_file->belong_to] as $file) {
      if ($this->this_info_file_is_of_the_this_file($file, $info_file)) {
       $info_file->file = $file;
      }
     }
    }
   }
  }
 }


 public static function responseStatusCode($code)
 {
  self::$statusCode = $code;
 }

 public static function response()
 {
  return response(self::$request_data)->setStatusCode(self::$statusCode);
 }
}
