import {createWebHistory, createRouter, createWebHashHistory} from "vue-router";

import dashboard_index from "../admin/pages/dashboard/index.vue"
import product_main from "../admin/pages/product/main.vue"
import product_list from "../admin/pages/product/list.vue"
import product_create from "../admin/pages/product/create.vue"
import product_edit from "../admin/pages/product/edit.vue"

const routes = [
  // {
  //   path: "/",
  //   name: "dashboard_index",
  //   component: dashboard_index
  // },
  {
    path: "/",
    name: "product_main",

    component: product_main,
    redirect: '/list',
    children:[
      {
        path: "list",
        name: "product_list",
        component: product_list,
      },
      {
        path: "create",
        name: "product_create",
        component: product_create,
      },
      {
        path: "edit/:id",
        name: "product_edit",
        component: product_edit,
      },

      // {
      //   path: "delete",
      //   name: "product.delete",
      //   component: { template: '<div>product.delete</div>' },
      // },
    ]
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  // history: createWebHistory(),
  routes,
});

export default router;