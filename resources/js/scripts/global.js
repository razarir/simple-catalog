let loading = {
  init(type) {
    if (type == 'btn') {
      btn_loading.start()
    }
  }
};

let btn_loading = {
  start() {
    $('.btn.activate').addClass('loading');
  },
  done() {
    $('.btn.activate').addClass('done');
    setTimeout(() => {
      $('.btn.activate').removeClass('loading done');
    }, 2000);
  },
  error() {
    $('.btn.activate').addClass('error');
    setTimeout(() => {
      $('.btn.activate').removeClass('loading error');
    }, 2000);
  },
  stop() {
    $('.btn.activate').removeClass('loading');
  }
};

let input_loading = {
  start() {
    $('.input-box.select-activate').addClass('loading');
  },
  stop() {
    $('.input-box.select-activate').removeClass('loading');
  }
};

let swal_alert = {
  success(text) {
    swal.fire({
      title: 'موفق',
      text: text,
      type: 'success',
      confirmButtonText: 'تایید'
    });
  },
  error(text) {
    swal.fire({
      title: 'خطا!',
      text: text,
      type: 'error',
      confirmButtonText: 'تایید'
    });
  },
  confirm(text) {
    return swal.fire({
      title: 'آیا اطمینان دارید؟',
      text: text,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'بله، حذف شود!',
      cancelButtonText: 'انصراف',
    });
  },
  delete(item) {
   return  this.confirm(item + ' حذف شود؟!')
  },
  error_for_all_code(error) {
    switch (error.response.status) {
      case 401:
        this.error('.احراز هویت نشده اید')
        break;
      case 403:
        this.error('.ورود ممنون می باشد')
        break;
      case 404:
        this.error('.پیدا نشد')
        break;
      case 419:
        this.error('.صفحه منقضی شده است.صفحه را رفرش کنید و دوباره وارد سایت شوید')
        break;
      case 422:
        this.error('اطلاعات خواسته شده بدرستی وارد نشده‌اند.')
        return error.response.data.errors;
        break;
      case 429:
        this.error('.تعداد درخواست ریادی فرستاده اید')
        break;
      case 500:
        this.error('.سرور خراب است')
        break;
      case 503:
        this.error('.سرور در دسترس نمیباشد')
        break;
      case 404:
        this.error('.پیدا نشد')
        break;
      default:
        this.error('خطا در ارتباط با سرور! لطفا اتصال اینترنت خود را بررسی کنید.');
        break;
    }
  },
  error_with_code(error) {
    switch (error) {
      case 401:
        this.error('.احراز هویت نشده اید')
        break;
      case 403:
        this.error('.ورود ممنون می باشد')
        break;
      case 419:
        this.error('.صفحه منقضی شده است.صفحه را رفرش کنید و دوباره وارد سایت شوید')
        break;
      case 422:
        this.error('اطلاعات خواسته شده بدرستی وارد نشده‌اند.')
        break;
      case 429:
        this.error('.تعداد درخواست ریادی فرستاده اید')
        break;
      case 500:
        this.error('.سرور خراب است')
        break;
      case 503:
        this.error('.سرور در دسترس نمیباشد')
        break;
      case 404:
        this.error('.پیدا نشد')
        break;
      default:
        this.error('خطا در ارتباط با سرور! لطفا اتصال اینترنت خود را بررسی کنید.');
        break;
    }
  },
};

let form_data = {
  form_data(form_data=null, files = null, _method = null) {

    let formData = new FormData();
    if (form_data !== null) {
      let let_form_data = {};
      for (const [key, value] of Object.entries(form_data)) {
        if ((value !== null && value !== '')) {
          if (Array.isArray(value) && value.length === 0) {
            continue;
          }
          let_form_data[key] = value;
        }
      }
      formData.append('data', JSON.stringify(let_form_data));
    }
    if (files !== null && files.length) {
      files.forEach((item) => {
        if (!files.upload && item.file) {
          formData.append('files' + '[]', item.file);

        }
      });
    }
    if (_method !== null) {
      formData.append('_method', _method);
    }
    return formData;
  }

}

export {btn_loading, input_loading, swal_alert, form_data};
