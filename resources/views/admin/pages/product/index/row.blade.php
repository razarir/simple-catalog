@php
 /** @var \App\Models\Product  $product*/
@endphp
{{--<tr>
 <th class="text-center">#</th>
 <th><i class="material-icons">image</i></th>
 <th>نام محصول</th>
 <th>دسته بندی</th>
 <th>قیمت</th>
 <th class="text-right">فعالیت</th>
</tr>--}}
<tr>
 <td class="text-center">{{$loop->iteration}}</td>
 <td><img src="{{$product->files[0]->address}}" alt="{{$product->files[0]->alt}}"></td>
 <td>{{$product->name}}</td>
 <td>{{$product->categories_implode}}</td>
 <td class="text-right">{{$product->get_price}}</td>
 <td class="td-actions text-right">
  <a href="{{route('admin.product.edit',[$product->id])}}">
   <button type="button" rel="tooltip" class="btn btn-success">
    <i class="material-icons">edit</i>
   </button>
  </a>
  <button type="submit" onclick="js_index.delete({{$product->id}})" rel="tooltip" class="btn btn-danger">
   <i class="material-icons">close</i>
  </button>
 </td>

</tr>