<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">@yield('title')</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end">
            <div class="navbar-form justify-content-start">

                <div class="input-group no-border">
                    <a class="nav-link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="material-icons">exit_to_app</i>
                        <h6 class="d-lg-none d-md-block">
                            {{ __('passwords.logout') }}

                        </h6>
                    </a>
                </div>

            </div>

            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                  style="display: none;">
                @csrf
            </form>


            <ul class="navbar-nav">
                <li class="nav-item">

                </li>
            </ul>
        </div>
    </div>
</nav>
