<head>
 <meta charset="utf-8"/>
 <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/public/img/icon.png')}}">
 <link rel="icon" type="image/png" href="{{asset('assets/public/img/icon.png')}}">
 <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
 <meta name="csrf-token" content="{{ csrf_token() }}">

 <title>
  @yield('title')
 </title>
 <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
       name='viewport'/>
 <!-- Extra details for Live View on GitHub Pages -->
 <!-- Canonical SEO -->
 <link rel="canonical" href="https://www.creative-tim.com/product/material-dashboard"/>
 <!--  Social tags      -->
 <meta name="keywords"
       content="creative tim, html dashboard, html css dashboard, web dashboard, bootstrap 4 dashboard, bootstrap 4, css3 dashboard, bootstrap 4 admin, material dashboard bootstrap 4 dashboard, frontend, responsive bootstrap 4 dashboard, free dashboard, free admin dashboard, free bootstrap 4 admin dashboard">
 <meta name="description"
       content="Material Dashboard is a Free Material Bootstrap Admin with a fresh, new design inspired by Google's Material Design.">
 <!-- Schema.org markup for Google+ -->
 <meta itemprop="name" content="Material Dashboard by Creative Tim">
 <meta itemprop="description"
       content="Material Dashboard is a Free Material Bootstrap Admin with a fresh, new design inspired by Google's Material Design.">
 <meta itemprop="image" content="https://s3.amazonaws.com/creativetim_bucket/products/50/opt_md_thumbnail.jpg">
 <!-- Twitter Card data -->
 <meta name="twitter:card" content="product">
 <meta name="twitter:site" content="@creativetim">
 <meta name="twitter:title" content="Material Dashboard by Creative Tim">
 <meta name="twitter:description"
       content="Material Dashboard is a Free Material Bootstrap Admin with a fresh, new design inspired by Google's Material Design.">
 <meta name="twitter:creator" content="@creativetim">
 <meta name="twitter:image" content="https://s3.amazonaws.com/creativetim_bucket/products/50/opt_md_thumbnail.jpg">
 <!-- Open Graph data -->
 <meta property="fb:app_id" content="655968634437471">
 <meta property="og:title" content="Material Dashboard by Creative Tim"/>
 <meta property="og:type" content="article"/>
 <meta property="og:url" content="https://demos.creative-tim.com/material-dashboard/examples/dashboard.html"/>
 <meta property="og:image" content="https://s3.amazonaws.com/creativetim_bucket/products/50/opt_md_thumbnail.jpg"/>
 <meta property="og:description"
       content="Material Dashboard is a Free Material Bootstrap Admin with a fresh, new design inspired by Google's Material Design."/>
 <meta property="og:site_name" content="Creative Tim"/>
 <!--     Fonts and icons     -->
 <link rel="stylesheet" type="text/css"
       href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">


 <!-- Markazi Text font include just for persian demo purpose, don't include it in your project -->
 <link href="https://fonts.googleapis.com/css?family=Cairo&amp;subset=arabic" rel="stylesheet">
 <!-- CSS Files -->
 <link href="{{ url('assets/admin') }}/css/material-dashboard.min.css?v=2.1.2" rel="stylesheet"/>
 <link href="{{ url('assets/admin') }}/css/material-dashboard-rtl.css?v=1.1" rel="stylesheet"/>
 <!-- CSS Just for demo purpose, don't include it in your project -->
 <link href="{{ url('assets/admin') }}/demo/demo.css" rel="stylesheet"/>
 <!-- Google Tag Manager -->
 <script>
 </script>
 <!-- End Google Tag Manager -->
 <!-- Style Just for persian demo purpose, don't include it in your project -->
 <style>


     ::-webkit-scrollbar {
         width: 10px;
         height: 10px;
     }

     ::-webkit-scrollbar-thumb {
         position: absolute;
         background-color: #aaa;
         border-radius: 6px;
         transition: background-color .2s linear, height .2s linear, width .2s ease-in-out, border-radius .2s ease-in-out;
         right: 2px;
         width: 6px;
     }

     ::-webkit-scrollbar-track {
         background-color: #ffffff;
         border-radius: 4px;
         -webkit-box-shadow: 0px 0px 3px 0px #888 inset;
         -moz-box-shadow: 0px 0px 3px 0px #888 inset;
         box-shadow: 0px 0px 3px 0px #888 inset;
     }

 </style>
 <link href="{{ asset('./font/persian.css') }}" rel="stylesheet"/>

</head>