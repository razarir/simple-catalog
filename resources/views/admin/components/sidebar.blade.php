<style>
    .sidebar .nav p {
        font-size: 16px;
    }
</style>
<div class="sidebar" data-color="azure" data-background-color="white"
     data-image="{{ url('assets/admin') }}/img/sidebar-2.jpg">
 <!--
   Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

   Tip 2: you can also add an image using data-image tag
-->
 <div class="logo"><a href="./index.html" class=" logo-mini  ">
   <img class="img-container" src="{{asset('assets/public/img/icon.png')}}" alt="">
  </a>
  <a href="./index.html" class="simple-text logo-normal">
   آمازون چوب
  </a></div>
 <div class="sidebar-wrapper">
  @php
   $current=Route::currentRouteName();
   $product=substr(Route::currentRouteName(),0,strlen('admin.product')) === 'admin.product';
  @endphp
  <ul class="nav">
   <li class="nav-item {{$current === 'admin.dashboard' ? 'active':''}} ">
    <a class="nav-link" href="{{route('admin.dashboard')}}">
     <i class="material-icons">dashboard</i>
     <p>داشبورد</p>
    </a>
   </li>
   <li class="nav-item {{ $product? 'active':''}} ">
    <a class="nav-link" data-toggle="collapse" href="#product">
     <p>
      <i class="material-icons">production_quantity_limits</i>
      محصولات
      <b class="caret"></b>
     </p>
    </a>
    <div class="collapse {{$product ? 'show':''}} " id="product">
     <ul class="nav">
      <li class="nav-item {{$current === 'admin.product.index' ? 'active':''}} ">
       <a class="nav-link" href="{{route('admin.product.index')}}">
        <span class="sidebar-mini"><i class="material-icons">list</i></span>
        <span class="sidebar-normal">لیست محصولات</span>
       </a>
      </li>
      <li class="nav-item {{$current === 'admin.product.create' ? 'active':''}} ">
       <a class="nav-link" href="{{route('admin.product.create')}}">
        <span class="sidebar-mini"><i class="material-icons">add_circle_outline</i></span>
        <span class="sidebar-normal">ثبت محصول جدید</span>
       </a>
      </li>
     </ul>
    </div>
   </li>
  </ul>
 </div>
</div>
