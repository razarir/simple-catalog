-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 07, 2021 at 06:07 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amazon_choob_3`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `code`, `parent`, `created_at`, `updated_at`) VALUES
(1, 'سیروس شفا', 'Impedit blanditiis id ratione rerum magni veniam. Placeat distinctio eum quibusdam minus possimus ex. Quidem doloremque recusandae vero unde consequatur est. Voluptas libero facere odit.', 'at', 1, '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(2, 'استاد بههن توسلی', 'Qui aperiam odio temporibus temporibus rerum tempore occaecati. Aut aut optio perferendis et. Vel sunt est quam odit ad qui optio. Perferendis quis pariatur deserunt quibusdam.', 'sed', 1, '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(3, 'فرینام ستاری', 'Reprehenderit voluptates veniam aut odit et ut. Est quia aut dicta. Necessitatibus et debitis quibusdam dolores.', 'quasi', 1, '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(4, 'صدف اعلم', 'Sit repellendus dolores repellendus facere. Architecto ea et voluptas voluptatibus et. Laborum dicta voluptatem nemo hic reprehenderit libero. Aut voluptatem necessitatibus sunt eos.', 'mollitia', 1, '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(5, 'اعظم حیاتی', 'Harum delectus natus est vitae. Rem et eos animi sunt omnis pariatur. Rerum corporis et sunt omnis expedita voluptatem.', 'neque', 1, '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(6, 'گلاره آهنگری', 'Quas commodi debitis deleniti sed. Dolore rerum perferendis delectus qui. Et accusantium voluptas sunt tempora.', 'quam', 1, '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(7, 'کیهان میرزاده', 'Reiciendis pariatur neque et laudantium. Non a est maxime mollitia earum et corrupti.', 'ipsa', 1, '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(8, 'اصغر ملک', 'Maiores corrupti aut quas voluptas est dolor. Maxime iure dolores est velit. Voluptate est accusantium aliquam error.', 'vero', 1, '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(9, 'سنجر انتظامی', 'Provident molestiae eveniet omnis placeat qui. Id sint voluptas qui dolor exercitationem est sunt. Quisquam et inventore laudantium molestiae ipsa mollitia.', 'similique', 1, '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(10, 'استاد ملکه جهان معارف', 'Quis aut vel ea et quis quibusdam voluptas. Eos dolorem reiciendis et itaque. Ipsa similique quos dolores dignissimos minus.', 'explicabo', 1, '2021-03-06 07:46:32', '2021-03-06 07:46:32');

-- --------------------------------------------------------

--
-- Table structure for table `category_product`
--

CREATE TABLE `category_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_product`
--

INSERT INTO `category_product` (`id`, `product_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 1, NULL, NULL),
(3, 3, 2, NULL, NULL),
(4, 4, 2, NULL, NULL),
(5, 5, 3, NULL, NULL),
(6, 6, 3, NULL, NULL),
(7, 7, 4, NULL, NULL),
(8, 8, 4, NULL, NULL),
(9, 9, 5, NULL, NULL),
(10, 10, 5, NULL, NULL),
(11, 11, 6, NULL, NULL),
(12, 12, 6, NULL, NULL),
(13, 13, 7, NULL, NULL),
(14, 14, 7, NULL, NULL),
(15, 15, 8, NULL, NULL),
(16, 16, 8, NULL, NULL),
(17, 17, 9, NULL, NULL),
(18, 18, 9, NULL, NULL),
(19, 19, 10, NULL, NULL),
(20, 20, 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Special` tinyint(4) DEFAULT NULL,
  `fileable_id` int(10) UNSIGNED NOT NULL,
  `fileable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `name`, `address`, `mime_type`, `size`, `thumbnail`, `extension`, `Special`, `fileable_id`, `fileable_type`, `created_at`, `updated_at`) VALUES
(1, 'فرنود داور', 'قزوین خیابان ندوشن ساختمان بهمنیار', 'vel', 'repellat', 'consequatur', 'voluptatum', 8, 1, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(2, 'سهیلا عصار', 'استان هرمزگان خیابان حکمت ساختمان سارنگ قطعه 33', 'deserunt', 'totam', 'numquam', 'eum', 5, 1, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(3, 'مهندس وارتان زارع', 'استان کهگیلویه و بویراحمد خیابان میزبانی ساختمان گلسان', 'amet', 'laborum', 'facere', 'doloribus', 7, 2, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(4, 'استاد جاوید صباغ', 'بوشهر خیابان زالی ساختمان رهادخت', 'placeat', 'nobis', 'sint', 'voluptatem', 7, 2, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(5, 'شیدفر مرتضوی', 'یزد خیابان آشوری ساختمان پروشات قطعه 99 کد پستی 5650748700', 'est', 'voluptate', 'voluptates', 'quod', 0, 3, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(6, 'غزاله شاملو', 'گلستان خیابان هوشیار ساختمان ارژنگ', 'similique', 'cumque', 'non', 'id', 0, 3, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(7, 'ویراف بزرگیان', 'قزوین خیابان شجاعی ساختمان مامیسا کد پستی 2086863257', 'deleniti', 'ut', 'fuga', 'in', 8, 4, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(8, 'آقای آدر انوری', 'استان آذربایجان غربی خیابان نواب ساختمان آیلین پلاک 77', 'totam', 'dolores', 'est', 'libero', 0, 4, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(9, 'نیکی ناز فروتن', 'اردبیل خیابان هاشمی ساختمان زینت قطعه 98', 'occaecati', 'qui', 'quisquam', 'sed', 9, 5, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(10, 'رسول میرزاده', 'کرمان خیابان درگاهی ساختمان مهرنام کد پستی 9231504943', 'non', 'maiores', 'commodi', 'qui', 4, 5, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(11, 'آقای شهاب غضنفری', 'استان خراسان شمالی خیابان زارع ساختمان آذرخش پلاک 94 کد پستی 2083100190', 'nemo', 'non', 'nihil', 'repellendus', 5, 6, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(12, 'استاد بهپور مددی', 'سمنان خیابان ضابطی ساختمان سمیر', 'reprehenderit', 'veritatis', 'ea', 'quisquam', 0, 6, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(13, 'هوشیار کوشکی', 'استان قزوین خیابان شریفیان ساختمان وهاب پلاک 20', 'ea', 'amet', 'et', 'qui', 6, 7, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(14, 'مهندس عفت میرسپاسی', 'یزد خیابان حیاتی ساختمان سیندخت', 'assumenda', 'consectetur', 'odio', 'non', 8, 7, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(15, 'فرجام مشا', 'مازندران خیابان معروف ساختمان شاوه کد پستی 7384946695', 'reiciendis', 'nobis', 'quasi', 'dolorem', 3, 8, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(16, 'مانی شریف', 'استان ایلام خیابان بهاور ساختمان شمیل', 'ut', 'expedita', 'amet', 'beatae', 2, 8, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(17, 'آمنه همدانی', 'استان اردبیل خیابان علی ساختمان جمیله پلاک 28', 'eveniet', 'fugit', 'voluptatum', 'excepturi', 1, 9, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(18, 'مهندس آمنه واعظی', 'یزد خیابان ابوذر ساختمان پروا', 'nobis', 'ab', 'ratione', 'dolorum', 7, 9, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(19, 'توتک زنجانی', 'خراسان رضوی خیابان بهشتی ساختمان سروین کد پستی 6319542771', 'voluptatibus', 'ipsa', 'vero', 'eius', 6, 10, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(20, 'اسماعیل شفا', 'مازندران خیابان چنگیزی ساختمان تیمور قطعه 31', 'debitis', 'sit', 'nam', 'omnis', 3, 10, 'product', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(21, 'دادمهر رنگرز', 'آذربایجان شرقی خیابان روستا ساختمان مهران', 'dolor', 'aut', 'voluptatem', 'et', 4, 11, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(22, 'شریفه عاشوری', 'آذربایجان غربی خیابان استادی ساختمان بهمیس', 'est', 'omnis', 'et', 'laborum', 0, 11, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(23, 'تینا پناهیان', 'استان اردبیل خیابان فنی‌زاده ساختمان آذرشین کد پستی 6788897501', 'velit', 'veniam', 'perferendis', 'fugiat', 5, 12, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(24, 'منیره اصفهانی', 'لرستان خیابان پایور ساختمان هاتف قطعه 59 کد پستی 8147517903', 'est', 'similique', 'nobis', 'qui', 6, 12, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(25, 'دکتر هوشان هومن', 'یزد خیابان عارف ساختمان کاظم', 'amet', 'dolorem', 'ut', 'quos', 1, 13, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(26, 'جمیله یثربی', 'سیستان و بلوچستان خیابان همدانی ساختمان بانویه', 'omnis', 'dolores', 'et', 'dolor', 6, 13, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(27, 'فرناد شادمهر', 'تهران خیابان قمیشی ساختمان طوفان', 'quo', 'magnam', 'quaerat', 'porro', 8, 14, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(28, 'آسام فاطمی', 'مازندران خیابان بهبهانی ساختمان ثریّا پلاک 94', 'et', 'eos', 'sint', 'aliquam', 2, 14, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(29, 'دری حائری', 'سیستان و بلوچستان خیابان میرزاده ساختمان آوند', 'nisi', 'voluptatem', 'aut', 'officia', 0, 15, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(30, 'مهندس ظریف زهرایی', 'تهران خیابان انتظامی ساختمان رخشا', 'doloremque', 'porro', 'velit', 'minus', 7, 15, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(31, 'دل افرز پیوندی', 'گلستان خیابان آریان‌پور ساختمان سیمبر قطعه 45 کد پستی 8418860658', 'nisi', 'aperiam', 'aperiam', 'temporibus', 0, 16, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(32, 'تراب فروتن', 'اصفهان خیابان حکمی ساختمان ایاز', 'nesciunt', 'cum', 'molestiae', 'excepturi', 3, 16, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(33, 'مونا گلپایگانی', 'کرمانشاه خیابان طبیب‌زاده ساختمان مژگان قطعه 82', 'vel', 'id', 'earum', 'harum', 6, 17, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(34, 'شهرود میدری', 'گلستان خیابان حبیبی ساختمان صبا پلاک 76 کد پستی 3144925249', 'dolores', 'asperiores', 'sapiente', 'sunt', 7, 17, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(35, 'دکتر پیمان ذاکری', 'کردستان خیابان موسویان ساختمان کامشاد پلاک 13', 'quia', 'ad', 'molestias', 'est', 3, 18, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(36, 'بهادر گلپایگانی', 'استان قم خیابان گلپایگانی ساختمان محمّد کد پستی 5639977752', 'rem', 'officia', 'vel', 'optio', 2, 18, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(37, 'مهندس دریاناز همت', 'استان آذربایجان غربی خیابان امانی ساختمان بلکا قطعه 55 کد پستی 5955019851', 'accusantium', 'autem', 'aut', 'iste', 4, 19, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(38, 'شورانگیز امانی', 'زنجان خیابان میدری ساختمان آرش پلاک 40 کد پستی 8828724916', 'culpa', 'at', 'pariatur', 'laborum', 7, 19, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(39, 'دریا دل فروتن', 'آذربایجان شرقی خیابان شرف ساختمان طلایه پلاک 48', 'quia', 'et', 'sed', 'vitae', 2, 20, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(40, 'رخساره صغیری', 'استان هرمزگان خیابان حقانی ساختمان میچکا قطعه 90 کد پستی 9397986013', 'veritatis', 'dicta', 'quaerat', 'eaque', 7, 20, 'product', '2021-03-06 07:46:33', '2021-03-06 07:46:33');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'دکتر مازیار ضرغامی', '808432480690378', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(2, 'مهیاز ظریف', '526076289683590', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(3, 'رودابه کیان', '99209843217455', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(4, 'مهندس فرشین پازارگاد', '465926952205506', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(5, 'آتوسا سرمد', '264382701540750', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(6, 'خانم آویز صفوی', '367834992838130', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(7, 'پرور فرامرزی', '963676728804772', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(8, 'شهناز راسخ', '267260532938233', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(9, 'فاخته همایون', '535238053755419', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(10, 'ایرج محمدی', '386911843278074', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(11, 'آقای رشین حقیقی', '208393545166059', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(12, 'حوّا عارف', '704170409223463', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(13, 'میهن یار باستانی', '67899030505185', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(14, 'سروناز شبستری', '592277583028172', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(15, 'ویدا چگنی', '507519004545491', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(16, 'فرزین زارع', '684645462712193', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(17, 'خانم نیکی شریعتی', '320877434035707', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(18, 'استاد کیانوش ندوشن', '213496475162225', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(19, 'دکتر خرداد حائری', '710867534158359', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(20, 'اندیشه علی‌زمانی', '983577325279564', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(21, 'sasasassa', 'dfhsfhsfhsdfh', '2021-03-06 07:59:43', '2021-03-06 07:59:43'),
(23, 'sasasassa', 'dfgsdgdsgdsf2021-03-06 11:33:22', '2021-03-06 08:03:22', '2021-03-06 08:03:22'),
(24, 'sasasassa', 'dfgsdgdsgdsf2021-03-06 11:33:42', '2021-03-06 08:03:42', '2021-03-06 08:03:42'),
(25, 'sasasassa', 'dfgsdgdsgdsf2021-03-06 11:34:08', '2021-03-06 08:04:08', '2021-03-06 08:04:08'),
(26, 'sasasassa', 'dfgsdgdsgdsf2021-03-06 11:34:43', '2021-03-06 08:04:43', '2021-03-06 08:04:43'),
(27, 'sasasassa', 'dfgsdgdsgdsf2021-03-06 11:37:04', '2021-03-06 08:07:04', '2021-03-06 08:07:04'),
(28, 'sasasassa', 'dfgsdgdsgdsf2021-03-06 11:38:11', '2021-03-06 08:08:11', '2021-03-06 08:08:11'),
(29, 'sasasassa', 'dfgsdgdsgdsf2021-03-07 04:21:14', '2021-03-07 00:51:14', '2021-03-07 00:51:14'),
(30, 'sasasassa', 'dfgsdgdsgdsf2021-03-07 04:22:18', '2021-03-07 00:52:18', '2021-03-07 00:52:18'),
(31, 'sasasassa', 'dfgsdgdsgdsf2021-03-07 04:22:46', '2021-03-07 00:52:46', '2021-03-07 00:52:46'),
(32, 'sasasassa', 'dfgsdgdsgdsf2021-03-07 04:23:17', '2021-03-07 00:53:17', '2021-03-07 00:53:17'),
(33, 'sasasassa', 'dfgsdgdsgdsf2021-03-07 04:23:46', '2021-03-07 00:53:46', '2021-03-07 00:53:46'),
(34, 'sasasassa', 'dfgsdgdsgdsf2021-03-07 04:24:04', '2021-03-07 00:54:04', '2021-03-07 00:54:04'),
(35, 'sasasassa', 'dfgsdgdsgdsf2021-03-07 04:25:41', '2021-03-07 00:55:41', '2021-03-07 00:55:41'),
(36, 'sasasassa', 'dfgsdgdsgdsf2021-03-07 04:25:50', '2021-03-07 00:55:50', '2021-03-07 00:55:50'),
(37, 'sasasassa', 'dfgsdgdsgdsf2021-03-07 04:26:15', '2021-03-07 00:56:15', '2021-03-07 00:56:15'),
(38, 'sasasassa', 'dfgsdgdsgdsf2021-03-07 04:27:29', '2021-03-07 00:57:29', '2021-03-07 00:57:29');

-- --------------------------------------------------------

--
-- Table structure for table `product_prop`
--

CREATE TABLE `product_prop` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `prop_id` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_prop`
--

INSERT INTO `product_prop` (`id`, `product_id`, `prop_id`, `value`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(2, 1, 2, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(3, 1, 3, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(4, 2, 4, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(5, 2, 5, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(6, 2, 6, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(7, 3, 7, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(8, 3, 8, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(9, 3, 9, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(10, 4, 10, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(11, 4, 11, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(12, 4, 12, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(13, 5, 13, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(14, 5, 14, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(15, 5, 15, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(16, 6, 16, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(17, 6, 17, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(18, 6, 18, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(19, 7, 19, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(20, 7, 20, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(21, 7, 21, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(22, 8, 22, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(23, 8, 23, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(24, 8, 24, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(25, 9, 25, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(26, 9, 26, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(27, 9, 27, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(28, 10, 28, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(29, 10, 29, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(30, 10, 30, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(31, 11, 31, 'سفید', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(32, 11, 32, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(33, 11, 33, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(34, 12, 34, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(35, 12, 35, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(36, 12, 36, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(37, 13, 37, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(38, 13, 38, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(39, 13, 39, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(40, 14, 40, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(41, 14, 41, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(42, 14, 42, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(43, 15, 43, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(44, 15, 44, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(45, 15, 45, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(46, 16, 46, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(47, 16, 47, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(48, 16, 48, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(49, 17, 49, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(50, 17, 50, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(51, 17, 51, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(52, 18, 52, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(53, 18, 53, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(54, 18, 54, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(55, 19, 55, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(56, 19, 56, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(57, 19, 57, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(58, 20, 58, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(59, 20, 59, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(60, 20, 60, 'سفید', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(61, 24, 62, 'srsddsgdf', '2021-03-06 08:03:42', '2021-03-06 08:03:42'),
(62, 25, 63, 'srsddsgdf', '2021-03-06 08:04:08', '2021-03-06 08:04:08'),
(63, 26, 64, 'srsddsgdf', '2021-03-06 08:04:43', '2021-03-06 08:04:43'),
(64, 28, 1, 'srsddsgdf', '2021-03-06 08:08:11', '2021-03-06 08:08:11'),
(65, 36, 80, 'srsddsgdf۱۱۱', '2021-03-07 00:55:50', '2021-03-07 00:55:50'),
(66, 37, 81, 'srsddsgdf۱۱۱', '2021-03-07 00:56:15', '2021-03-07 00:56:15'),
(67, 37, 82, '۲۳۴۲۳', '2021-03-07 00:56:15', '2021-03-07 00:56:15'),
(68, 38, 83, 'srsddsgdf۱۱۱', '2021-03-07 00:57:29', '2021-03-07 00:57:29'),
(69, 38, 84, '۲۳۴۲۳', '2021-03-07 00:57:29', '2021-03-07 00:57:29');

-- --------------------------------------------------------

--
-- Table structure for table `props`
--

CREATE TABLE `props` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `props`
--

INSERT INTO `props` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'آقای نوین اقلیما', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(2, 'همای صانعی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(3, 'کتیبه ترکاشوند', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(4, 'مروان ذاکری', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(5, 'شمیا مرادخانی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(6, 'خورشید بانو شعبانی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(7, 'حدا نعمت‌زاده', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(8, 'کامبخش آدینه', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(9, 'جهانبخت شریعتی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(10, 'ناناز مفتح', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(11, 'جهانبخت نوبختی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(12, 'فردین باهنر', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(13, 'فرانه مظفر', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(14, 'فرلاس اللهیاری', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(15, 'مهندس نویسه پویان', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(16, 'دانا گل', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(17, 'آروشا صدیقی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(18, 'وشمگیر سادات', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(19, 'یزدان بهشتی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(20, 'نرجس ضرغامی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(21, 'دادفر واعظ', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(22, 'مزدا فروتن', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(23, 'آمیتریس علی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(24, 'تابا ستاری', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(25, 'دکتر فرنود اعلم', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(26, 'ساتراپ اعتبار', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(27, 'لادن فریاد', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(28, 'کواد انوار', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(29, 'گلی سبحانی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(30, 'حریره ساعی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(31, 'باشو عارف', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(32, 'کواد واعظ‌زاده', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(33, 'افشک کاملی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(34, 'نامور هاشمیان', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(35, 'فرداد سبزواری', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(36, 'خانم شهرزاد هوشیار', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(37, 'سیامک باستانی', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(38, 'لادن سحاب', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(39, 'انیس هومن', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(40, 'جهاندار مستوفی', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(41, 'استاد مارال ابطحی', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(42, 'مهندس همراز پناهیان', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(43, 'ویشتاسب فولادوند', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(44, 'شادورد ابوذر', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(45, 'صبا اصلانی', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(46, 'خانم استر جنتی', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(47, 'مهین بانو خوئینی‌ها', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(48, 'سینا آهنگر', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(49, 'قادر دری', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(50, 'پسند پازوکی', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(51, 'تابش خامنه‌ای', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(52, 'مهندس جهانگیر آشوری', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(53, 'صادق نامور', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(54, 'پارمیس دانایی‌فر', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(55, 'مهوند دستغیب', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(56, 'نیلگون معروف', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(57, 'توسکا کاکاوند', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(58, 'حامیه پناهنده', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(59, 'مهندس ارنواز عارف', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(60, 'بوسه کوشکی', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(61, 'رنگ', '2021-03-06 07:59:43', '2021-03-06 07:59:43'),
(62, 'رنگ', '2021-03-06 08:03:42', '2021-03-06 08:03:42'),
(63, 'رنگ', '2021-03-06 08:04:08', '2021-03-06 08:04:08'),
(64, 'رنگ', '2021-03-06 08:04:43', '2021-03-06 08:04:43'),
(65, 'رنگ', '2021-03-06 08:07:04', '2021-03-06 08:07:04'),
(66, 'رنگ', '2021-03-06 08:08:11', '2021-03-06 08:08:11'),
(67, 'رنگ', '2021-03-07 00:51:14', '2021-03-07 00:51:14'),
(68, 'رنگ', '2021-03-07 00:52:18', '2021-03-07 00:52:18'),
(69, 'وزن', '2021-03-07 00:52:18', '2021-03-07 00:52:18'),
(70, 'رنگ', '2021-03-07 00:52:46', '2021-03-07 00:52:46'),
(71, 'وزن', '2021-03-07 00:52:46', '2021-03-07 00:52:46'),
(72, 'رنگ', '2021-03-07 00:53:17', '2021-03-07 00:53:17'),
(73, 'وزن', '2021-03-07 00:53:17', '2021-03-07 00:53:17'),
(74, 'رنگ', '2021-03-07 00:53:46', '2021-03-07 00:53:46'),
(75, 'وزن', '2021-03-07 00:53:46', '2021-03-07 00:53:46'),
(76, 'رنگ', '2021-03-07 00:54:04', '2021-03-07 00:54:04'),
(77, 'وزن', '2021-03-07 00:54:04', '2021-03-07 00:54:04'),
(78, 'رنگ', '2021-03-07 00:55:41', '2021-03-07 00:55:41'),
(79, 'وزن', '2021-03-07 00:55:41', '2021-03-07 00:55:41'),
(80, 'رنگ', '2021-03-07 00:55:50', '2021-03-07 00:55:50'),
(81, 'رنگ', '2021-03-07 00:56:15', '2021-03-07 00:56:15'),
(82, 'وزن', '2021-03-07 00:56:15', '2021-03-07 00:56:15'),
(83, 'رنگ', '2021-03-07 00:57:29', '2021-03-07 00:57:29'),
(84, 'وزن', '2021-03-07 00:57:29', '2021-03-07 00:57:29');

-- --------------------------------------------------------

--
-- Table structure for table `tagables`
--

CREATE TABLE `tagables` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL,
  `tagable_id` int(10) UNSIGNED NOT NULL,
  `tagable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tagables`
--

INSERT INTO `tagables` (`id`, `tag_id`, `tagable_id`, `tagable_type`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'product', NULL, NULL),
(2, 2, 1, 'product', NULL, NULL),
(3, 3, 2, 'product', NULL, NULL),
(4, 4, 2, 'product', NULL, NULL),
(5, 5, 3, 'product', NULL, NULL),
(6, 6, 3, 'product', NULL, NULL),
(7, 7, 4, 'product', NULL, NULL),
(8, 8, 4, 'product', NULL, NULL),
(9, 9, 5, 'product', NULL, NULL),
(10, 10, 5, 'product', NULL, NULL),
(11, 11, 6, 'product', NULL, NULL),
(12, 12, 6, 'product', NULL, NULL),
(13, 13, 7, 'product', NULL, NULL),
(14, 14, 7, 'product', NULL, NULL),
(15, 15, 8, 'product', NULL, NULL),
(16, 16, 8, 'product', NULL, NULL),
(17, 17, 9, 'product', NULL, NULL),
(18, 18, 9, 'product', NULL, NULL),
(19, 19, 10, 'product', NULL, NULL),
(20, 20, 10, 'product', NULL, NULL),
(21, 21, 11, 'product', NULL, NULL),
(22, 22, 11, 'product', NULL, NULL),
(23, 23, 12, 'product', NULL, NULL),
(24, 24, 12, 'product', NULL, NULL),
(25, 25, 13, 'product', NULL, NULL),
(26, 26, 13, 'product', NULL, NULL),
(27, 27, 14, 'product', NULL, NULL),
(28, 28, 14, 'product', NULL, NULL),
(29, 29, 15, 'product', NULL, NULL),
(30, 30, 15, 'product', NULL, NULL),
(31, 31, 16, 'product', NULL, NULL),
(32, 32, 16, 'product', NULL, NULL),
(33, 33, 17, 'product', NULL, NULL),
(34, 34, 17, 'product', NULL, NULL),
(35, 35, 18, 'product', NULL, NULL),
(36, 36, 18, 'product', NULL, NULL),
(37, 37, 19, 'product', NULL, NULL),
(38, 38, 19, 'product', NULL, NULL),
(39, 39, 20, 'product', NULL, NULL),
(40, 40, 20, 'product', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'ارژنگ رحماندوست', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(2, 'تیرگر فرامرزی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(3, 'مهندس توریا عصار', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(4, 'به بها استادی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(5, 'پوران اقلیما', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(6, 'شاهزاده اشتری', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(7, 'الماس مهاجرانی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(8, 'دکتر فرخنده دباغ', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(9, 'راضیه روزبه', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(10, 'روشان مستوفی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(11, 'دکتر پدرام انتظامی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(12, 'سمیر زنوزی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(13, 'جهانبان کیمیایی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(14, 'همایون استادی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(15, 'رادمان قانعی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(16, 'استاد الیا ضابطی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(17, 'خانم همراز ابوذر', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(18, 'نیّره افخم', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(19, 'کریمان فولادوند', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(20, 'شیواد کاملی', '2021-03-06 07:46:32', '2021-03-06 07:46:32'),
(21, 'آراد میرباقری', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(22, 'خانم گلشاد تهرانی', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(23, 'زادفر پناهنده', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(24, 'رشیا شریف', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(25, 'جهانفر ملکیان', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(26, 'دکتر روناک پورنگ', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(27, 'مهندس تاجفر وکیلی', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(28, 'استاد محبوبه ترکاشوند', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(29, 'شادرخ آشنا', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(30, 'سینام صفوی', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(31, 'احترام عزیزی', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(32, 'ابریشم انتظامی', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(33, 'اردون اعتبار', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(34, 'مستانه مصباح‌زاده', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(35, 'رخشان حجتی', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(36, 'فردیس لنکرانی', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(37, 'هومین فرمانفرمائیان', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(38, 'مهندس ساناز پایدار', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(39, 'مردآویج الهام', '2021-03-06 07:46:33', '2021-03-06 07:46:33'),
(40, 'نادی اعلم', '2021-03-06 07:46:33', '2021-03-06 07:46:33');

-- --------------------------------------------------------

--
-- Table structure for table `telescope_entries`
--

CREATE TABLE `telescope_entries` (
  `sequence` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `should_display_on_index` tinyint(1) NOT NULL DEFAULT 1,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `telescope_entries`
--

INSERT INTO `telescope_entries` (`sequence`, `uuid`, `batch_id`, `family_hash`, `should_display_on_index`, `type`, `content`, `created_at`) VALUES
(175, '92e45439-322e-4c46-82cb-6c0c0cce3b77', '92e45439-373c-49c2-95f3-ceea2d870176', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"delete from `telescope_entries`\",\"time\":\"18.26\",\"slow\":false,\"file\":\"C:\\\\xampp\\\\htdocs\\\\amazon_choob\\\\artisan\",\"line\":37,\"hash\":\"028161d6b6379f4e627c4ba6981fd441\",\"hostname\":\"DESKTOP-OTO8S93\"}', '2021-03-07 05:07:12'),
(176, '92e45439-367d-4ad1-8376-ed8911f1fc19', '92e45439-373c-49c2-95f3-ceea2d870176', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"delete from `telescope_monitoring`\",\"time\":\"0.56\",\"slow\":false,\"file\":\"C:\\\\xampp\\\\htdocs\\\\amazon_choob\\\\artisan\",\"line\":37,\"hash\":\"93d47a4a096729228b0389a4805074ec\",\"hostname\":\"DESKTOP-OTO8S93\"}', '2021-03-07 05:07:12'),
(177, '92e45439-36f8-4840-9d5b-cbb3ff6d56c3', '92e45439-373c-49c2-95f3-ceea2d870176', NULL, 1, 'command', '{\"command\":\"telescope:clear\",\"exit_code\":0,\"arguments\":{\"command\":\"telescope:clear\"},\"options\":{\"help\":false,\"quiet\":false,\"verbose\":false,\"version\":false,\"ansi\":false,\"no-ansi\":false,\"no-interaction\":false,\"env\":null},\"hostname\":\"DESKTOP-OTO8S93\"}', '2021-03-07 05:07:12');

-- --------------------------------------------------------

--
-- Table structure for table `telescope_entries_tags`
--

CREATE TABLE `telescope_entries_tags` (
  `entry_uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `telescope_monitoring`
--

CREATE TABLE `telescope_monitoring` (
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_parent_foreign` (`parent`);

--
-- Indexes for table `category_product`
--
ALTER TABLE `category_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_product_product_id_foreign` (`product_id`),
  ADD KEY `category_product_category_id_foreign` (`category_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_code_unique` (`code`);

--
-- Indexes for table `product_prop`
--
ALTER TABLE `product_prop`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_prop_product_id_foreign` (`product_id`),
  ADD KEY `product_prop_prop_id_foreign` (`prop_id`);

--
-- Indexes for table `props`
--
ALTER TABLE `props`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tagables`
--
ALTER TABLE `tagables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tagables_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `telescope_entries`
--
ALTER TABLE `telescope_entries`
  ADD PRIMARY KEY (`sequence`),
  ADD UNIQUE KEY `telescope_entries_uuid_unique` (`uuid`),
  ADD KEY `telescope_entries_batch_id_index` (`batch_id`),
  ADD KEY `telescope_entries_family_hash_index` (`family_hash`),
  ADD KEY `telescope_entries_created_at_index` (`created_at`),
  ADD KEY `telescope_entries_type_should_display_on_index_index` (`type`,`should_display_on_index`);

--
-- Indexes for table `telescope_entries_tags`
--
ALTER TABLE `telescope_entries_tags`
  ADD KEY `telescope_entries_tags_entry_uuid_tag_index` (`entry_uuid`,`tag`),
  ADD KEY `telescope_entries_tags_tag_index` (`tag`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `category_product`
--
ALTER TABLE `category_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=282;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=562;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=301;

--
-- AUTO_INCREMENT for table `product_prop`
--
ALTER TABLE `product_prop`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=853;

--
-- AUTO_INCREMENT for table `props`
--
ALTER TABLE `props`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=868;

--
-- AUTO_INCREMENT for table `tagables`
--
ALTER TABLE `tagables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=562;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=562;

--
-- AUTO_INCREMENT for table `telescope_entries`
--
ALTER TABLE `telescope_entries`
  MODIFY `sequence` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_foreign` FOREIGN KEY (`parent`) REFERENCES `categories` (`id`);

--
-- Constraints for table `category_product`
--
ALTER TABLE `category_product`
  ADD CONSTRAINT `category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_prop`
--
ALTER TABLE `product_prop`
  ADD CONSTRAINT `product_prop_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_prop_prop_id_foreign` FOREIGN KEY (`prop_id`) REFERENCES `props` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tagables`
--
ALTER TABLE `tagables`
  ADD CONSTRAINT `tagables_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `telescope_entries_tags`
--
ALTER TABLE `telescope_entries_tags`
  ADD CONSTRAINT `telescope_entries_tags_entry_uuid_foreign` FOREIGN KEY (`entry_uuid`) REFERENCES `telescope_entries` (`uuid`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
