<?php

namespace Database\Factories;

use App\Models\Tagable;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class TagableFactory extends Factory
{
 protected $model = Tagable::class;

 /**
  * Define the model's default state.
  *
  * @return array
  */
 public function definition()
 {
  return [
   'created_at' => Carbon::now(),
   'updated_at' => Carbon::now(),
   'tag_id' => $this->faker->randomNumber(),
   'tagable_id' => $this->faker->randomNumber(),
   'tagable_type' => $this->faker->word,
   'value' => $this->faker->word,
  ];
 }
}
