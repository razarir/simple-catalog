<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class ProductFactory extends Factory
{
 protected $model = Product::class;

 /**
  * Define the model's default state.
  *
  * @return array
  */
 public function definition()
 {
//  $values = array();
//  for ($i = 0; $i < 10; $i++) {
//   // get a random digit, but always a new one, to avoid duplicates
//   $values []= $this->faker->unique()->randomDigit;
//  }
  return [
   'created_at' => Carbon::now(),
   'updated_at' => Carbon::now(),
   'name' => $this->faker->name,
   'description' => $this->faker->text,
   'code' => $this->faker->unique()->numberBetween(1, 999999999999999),
   'price' => $this->faker->numerify()
  ];
 }
}
