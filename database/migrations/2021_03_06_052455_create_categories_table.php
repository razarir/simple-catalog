<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesTable extends Migration {

	public function up()
	{
		Schema::create('categories', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 255);
			$table->text('description')->nullable();
			$table->string('code', 255)->nullable();
			$table->integer('parent')->unsigned()->nullable()->comment('فعلا غیر قابل استفاده است.');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('categories');
	}
}