<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration
{

 public function up()
 {
  Schema::create('products', function (Blueprint $table) {
   $table->increments('id');
   $table->string('code', 255)->unique();
   $table->string('name', 255);
   $table->string('price');
   $table->text('description');
   $table->timestamps();
  });
 }

 public function down()
 {
  Schema::drop('products');
 }
}