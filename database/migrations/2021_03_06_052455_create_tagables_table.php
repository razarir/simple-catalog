<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTagablesTable extends Migration {

	public function up()
	{
		Schema::create('tagables', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('tag_id')->unsigned();
			$table->integer('tagable_id')->unsigned();
			$table->string('tagable_type');
			$table->string('value');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('tagables');
	}
}